<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prnt extends Model
{
    //
    protected $table = 'prnts';
    //
    public $primaryKey = 'id';
    //
    public $timestamps = true;
}
