<?php

use Illuminate\Database\Seeder;

class PrntSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Prnt::class, 25)->create();
    }
}
