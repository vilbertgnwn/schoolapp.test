<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        //
        'email' => $faker->email(),
        'password' => $faker->password(),
        'firstname' => $faker->firstName(),
        'lastname' => $faker->lastName(),
        'dob' => $faker->dateTimeThisDecade(),
        'phone' => $faker->phoneNumber(),
        'mobile' => $faker->e164PhoneNumber(),
        'status' => 1,
        'last_login' => $faker->dateTimeThisMonth(),
        'last_login_ip' => $faker->ipv4()
        //
    ];
});
